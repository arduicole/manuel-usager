# Utilisation du matériel et maintenance

## Entrées et sorties du circuit contrôleur

![](./images/pinout_nodemcu.png)

## Diagrammes et composants

### Diagramme schématique

### Diagramme matriciel

### Liste des composants

## Utilisation du mode manuel

<!-- Besoin d'une photo du contrôleur -->

## Mesures de sécurité

## Configuration et programmation du contrôleur
