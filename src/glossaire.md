# Glossaire

Cette section définit certains termes techniques employés dans ce manuel.

## Capteur

Appareil de mesure d'un phénomène physique.  
Ils existent sous différentes formes et permettent de mesurer toutes sortes de phénomènes tels que la température, l'humidité, l'ensoleillement, etc.

## Actionneur

Appareil que l'on peut contrôler ou, en d'autres mots, actionner.

## Asservissement

Stabilisation automatique de certaines conditions environnantes en fonction d'une consigne prédéterminée par l'utilisateur.
