# Fonctionnalités du produit

Le contrôleur intègre les principales fonctionnalités décrites ci-dessous.

## Contrôle manuel des actionneurs

Lorsque nécessaire, l'agriculteur peut manuellement contrôler la fournaise, la ventilation et la position des côtés ouvrants de la serre de 2 manière différentes.

### Interrupteurs

La première manière utilise les interrupteurs qui se trouvent sur la face avant du boîtier du contrôleur.
Lorsque vous êtes à côté de votre contrôleur, vous pouvez les utiliser pour contrôler chacun des actionneurs.

### Application mobile

Lorsque vous n'êtes pas à côtés de votre contrôleur, vous pouvez toujours utiliser l'application mobile pour contrôler l'état des actionneurs, de la même manière qu'avec les interrupteurs.

## Asservissement de la température

À partir de l'application mobile, vous pourrez configurer des températures pour chacune de vos serres en fonction de l'ensoleillement de la journée.  
Vous pourrez ensuite rapidement décider chaque jour de laquelle choisir en fonction de l'ensoleillement.

Il y a trois niveaux d'ensoleillement disponible: ensoileillé, mi-nuageux et nuageux.

Le contrôleur se chargera ensuite de contrôler votre fournaise, votre ventilateur et vos côtés ouvrants afin de s'assurer que la température reste à la consigne configurée.

## Lecture en temps réel, contrôle à distance et configuration de l'asservissement

L'application vous permet également de lire les températures en temps réel, de contrôler à distance les actionneurs de la
serre au besoin, et de configurer les différentes consignes et paramètres pour l'asservissement.

## Enregistrement des données sur un service de stockage infonuagique

Les données recueillies par le contrôleur peuvent ensuite être enregistrées sur un service de stockage infonuagique (cloud)
pour consultation ultérieure et statistiques additionnelles.
