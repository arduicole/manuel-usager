# Table des matières

- [Introduction](./introduction.md)
- [Fonctionnalités](./fonctionnalites.md)
- [Configuration initiale du contrôleur](./config-init/index.md)

  - [Configurer votre Google Drive](./config-init/1-config-gdrive.md)
  - [Configurer votre ordinateur](./config-init/2-config-ordi.md)
  - [Générer une clé d'encryption](./config-init/3-gen-encryption.md)
  - [Modifier la configuration de base du contrôleur](./config-init/4-modifier-config-base.md)
  - [Compiler et téléverser le code](./config-init/5-compiler-televerser.md)
  - [Tester le mode manuel](./config-init/6-test-mode-manuel.md)
  - [Configurer les rollups](./config-init/7-config-rollups.md)
  - [Configurer votre routeur](./config-init/8-config-routeur.md)

- [Matériel et maintenance](./materiel.md)
- [Application mobile](./app/index.md)

  - [Installation et configuration initiale](./app/config-init.md)
  - [Configurer une nouvelle serre](./app/config-serre.md)
  - [Mode de contrôle à distance](./app/ctrl-distance/index.md)

    - [Configurer et activer](./app/ctrl-distance/config-et-activer.md)
    - [Désactiver ce mode](./app/ctrl-distance/desactiver.md)

  - [Mode de cycle](./app/cycle/index.md)

    - [Créer un cycle](./app/cycle/creer.md)
    - [Modifier un cycle](./app/cycle/modifier.md)
    - [Exécuter un cycle](./app/cycle/executer.md)
    - [Visionner un cycle en exécution](./app/cycle/visionner.md)
    - [Arrêter un cycle en exécution](./app/cycle/arreter.md)

  - [Mode d'asservissement](./app/asservissement/index.md)

    - [Activer](./app/asservissement/activer.md)
    - [Configurer le niveau d'ensoleillement](./app/asservissement/config-ensoleillement.md)
    - [Configurer une consigne](./app/asservissement/config-consigne.md)

  - [Alertes](./app/alertes/index.md)

    - [Configurer l'intervalle de vérification des alertes](./app/alertes/config-verif.md)
    - [Configurer les notifications](./app/alertes/config-notifs.md)
    - [Voir la liste d'alertes](./app/alertes/visionner.md)
    - [Supprimer des alertes](./app/alertes/supprimer.md)

- [Configuration de Google Sheets](./configuration-google-sheets.md)
- [Glossaire](./glossaire.md)
