# Configuration initiale du contrôleur

Si votre contrôleur n'a pas été initialisé et configuré par Arduicole, cette section du manuel vous expliquera comment le faire vous-même en suivant ces étapes suivantes:

- [Configurer votre Google Drive](./1-config-gdrive.md)
- [Configurer votre ordinateur](./2-config-ordi.md)
- [Générer une clé d'encryption](./3-gen-encryption.md)
- [Modifier la configuration de base du contrôleur](./4-modifier-config-base.md)
- [Compiler et téléverser le code](./5-compiler-televerser.md)
- [Tester le mode manuel](./6-test-mode-manuel.md)
- [Configurer les rollups](./7-config-rollups.md)
- [Configurer votre routeur](./8-config-routeur.md)
