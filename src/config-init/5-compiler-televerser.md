## Compiler et téléverser le code

- Retirer le NodeMCU du contrôleur.

  - Appuyer sur le bouton d'arrêt d'urgence du contrôleur pour le mettre hors tension.
  - Débrancher le contrôleur du 120VAC et du 24VDC.
  - Ouvrir le contrôleur.
  - Retirer délicatement le NodeMCU du contrôleur.

- Connecter le NodeMCU à l'ordinateur avec un câble USB Micro B.
- Ouvrir le [Arduino IDE][arduino ide].
- Ouvrir le code source Arduino en naviguant à `File > Open...` à partir de la barre des tâches.
- Appuyer sur le bouton `Upload` pour compiler et téléverser le code sur votre NodeMCU.

  **ATTENTION!** Ne pas débrancher le NodeMCU tant que le téléversement n'est pas terminé.

  ![](../images/upload.png)

- Replacer le NodeMCU dans le contrôleur.

  - Appuyer sur le bouton d'arrêt d'urgence du contrôleur.
  - S'assurer que le contrôleur est encore débranché du 120VAC et du 24VDC.
  - Ouvrir le contrôleur.
  - Replacer délicatement le NodeMCU dans le contrôleur.

    **ATTENTION!** Ne pas placer le NodeMCU dans la mauvaise orientation. Il faut que son port USB soit orienté vers le haut.  
    Il faut également que toutes ses broches soient bien placées dans les trous de son connecteur. Aucune patte ne doit dépasser en haut ou en bas du connecteur.

    ![](../images/orientation_nodemcu.png)

  - Rebrancher le contrôleur au 120VAC et au 24VDC.
  - Mettre le contrôleur sous tension en tournant le bouton d'arrêt d'urgence dans le sens des aiguilles d'une montre.

[arduino ide]: https://www.arduino.cc/en/main/software
