## Configurer votre ordinateur

- Installer le [Arduino IDE][arduino ide] sur votre ordinateur.

  Des instructions pour différentes plateformes peuvent être consultées en anglais en cliquant ici:

  - [Windows](https://www.arduino.cc/en/guide/windows)
  - [macOS X](https://www.arduino.cc/en/Guide/MacOSX)
  - [Linux](https://www.arduino.cc/en/Guide/Linux)

- Configurer [Arduino IDE][arduino ide] afin qu'il puisse correctement téléverser le code sur le NodeMCU:

  - Cliquer sur `File > Preferences` à partir de la barre des tâches.

    Ceci devrait ouvrir une fenêtre permettant d'éditer les préférences de l'application.

    ![](../images/preferences.png)

  - Écrire le texte suivant dans le champ de texte `Additional Boards Manager URLs`: `http://arduino.esp8266.com/stable/package_esp8266com_index.json`.

  - Fermer la fenêtre de préférences en appuyant sur le bouton `OK`.

  - Se rendre dans `Tools > Board > Boards Manager` à partir de la barre des tâches.

    Ceci devrait ouvrir une nouvelle fenêtre.

  - Entrer `esp8266` dans la barre de recherche de la nouvelle fenêtre.

    ![](../images/boards_manager.png)

  - Appuyer sur le bouton `Install`.

    Attendre que l'installation soit terminée.

  - Fermer la fenêtre `Boards Manager`.

  - Sélectionner la board `NodeMCUv1.0` dans le menu `Tools > Board` de la barre des tâches.

    ![](../images/select_board.png)

  - Sélectionner `160MHz` comme vitesse du processeur.

    ![](../images/vitesse_processeur.png)

    Ceci double la vitesse du processeur.  
    Comme les communications entre l'application mobile et le contrôleur sont encryptées et que l'encryption est une tâche qui prend beaucoup de temps au processeur, doubler la vitesse du processeur accélère grandement la vitesse de communication entre le contrôleur et l'application mobile, ce qui offre une bien meilleure expérience d'utilisation.

- Installer les dépendances.

  Ce projet dépend de plusieurs librairies Arduino:

  - [DHTesp](https://github.com/beegee-tokyo/DHTesp)
  - [ArduinoJson](https://github.com/bblanchon/ArduinoJson)

  Vous devez les installer afin de pouvoir compiler ce projet.

  - Se rendre dans `Tools > Manage Libraries` à partir de la barre des tâches.

    Vous pouvez également utiliser le raccourci clavier `Ctrl+Shift+I`.

    Ceci va ouvrir une nouvelle fenêtre.

  - Pour chaque librairie à installer:

    - Entrer le nom de la librairie dans la barre de recherche.

      Utiliser les noms exacts donnés plus haut.

    - Appuyer sur le bouton `Install` de la librairie la plus haute dans la liste de résultats.

    ![](../images/installer_lib.png)

[arduino ide]: https://www.arduino.cc/en/main/software
