## Tester le mode manuel

- Placer le contrôleur en mode manuel.
- Allumer et éteindre la fournaise avec l'interrupteur pour vous assurer qu'elle fonctionne.
- Allumer et éteindre le ventilateur avec l'interrupteur pour vous assurer qu'il fonctionne.
- Fermer complètement le rollup sélectionné.
- Ouvrir complètement le rollup sélectionné en mesurant et en notant le temps qu'il prend.
- Fermer complètement le rollup sélectionné en mesurant et en notant le temps qu'il prend.
- Ouvrir de nouveau complètement le rollup sélectionné.
- Fermer le rollup et l'arrêter à 50% et mesurer le temps qu'il prend pour s'arrêter.
- Sélectionner l'autre rollup.
- Fermer complètement le rollup sélectionné.
- Ouvrir complètement le rollup sélectionné en mesurant et en notant le temps qu'il prend.
- Fermer complètement le rollup sélectionné en mesurant et en notant le temps qu'il prend.
- Ouvrir de nouveau complètement le rollup sélectionné.
- Fermer le rollup et l'arrêter à 50% et mesurer le temps qu'il prend pour s'arrêter.
