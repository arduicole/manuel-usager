## Modifier la configuration de base du contrôleur

Le fichier `Config.h`, dont vous pouvez voir un extrait ci-dessous, contient des options de configuration du contrôleur.  
Si vous n'êtes pas confortable avec la programmation en C/C++, ce fichier peut avoir l'air intimidant. Cependant, il n'y a rien de très compliqué dans ce fichier. Il contient seulement les options de configuration du programme.

![](../images/config_reseau.png)

Dans ce fichier, toutes les lignes qui commencent par `//` ou `//!` sont des lignes de commentaire. Ne modifiez pas ces lignes. Elles servent uniquement à expliquer à quoi servent les lignes qui les suivent.  
Toutes les lignes qui commencent par `#define` sont celles que vous allez modifier. Après le `#define` se trouve un nom que, comme les commentaires, vous ne devez pas modifier.

Voici la liste des paramètres à modifier:

- Nom et mot de passe WiFi

  **ATTENTION!** Le réseau WiFi doit être un réseau 2.4GHz avec une sécurité WPA ou WPA2. Le contrôleur n'est pas compatible avec les réseaux 5GHz ou avec des réseaux avec une sécurité autre que [WPA ou WPA2][wpa]. Il est possible que d'autres types de sécurité fonctionne, mais elles n'ont pas été testées.  
  Vous pouvez contacter votre fournisseur d'accès Internet pour obtenir de l'aide pour configurer votre point d'accès WiFi pour qu'il utilise [WPA ou WPA2][wpa] et pour activer le réseau 2.4GHz.

  Le premier paramètre à modifier est le nom de votre réseau WiFi.
  Il doit se trouver entre des guillemets anglais (`"`) après la ligne qui commence par `#define NOM_WIFI`.  
  La valeur par défaut de cette ligne est la suivante: `#define NOM_WIFI "Votre Réseau"`. Vous devez remplacer `Votre Réseau` par le nom de votre réseau WiFi. Ce nom doit contenir les mêmes minuscules, majuscules et symboles que votre réseau.  
  Si votre réseau s'appelle `Chez Raymond`, modifier la ligne pour la valeur suivante: `#define NOM_WIFI "Chez Raymond"`.

  Ensuite, modifier le nom du mot de passe du WiFi. Vous devez l'inscrire à la ligne commençant par `#define MOT_DE_PASSE_WIFI`. Vous devez encore une fois le placer entre des guillemets anglais.  
  La valeur par défaut de cette ligne est `#define MOT_DE_PASSE_WIFI "Votre mot de passe"`. Remplacer `Votre mot de passe` par votre mot de passe.  
  Si votre mot de passe est `Mot de passe sécuritaire 123`, modifier la ligne pour la valeur suivante: `#define MOT_DE_PASSE_WIFI "Mot de passe sécuritaire 123"`.

- Nom du point d'accès

  Ceci défini le nom du point d'accès créer par le contrôleur lorsqu'il n'arrive pas à se connecter à votre WiFi.  
  Il se trouve à la ligne commençant par `#define NOM_POINT_ACCES`.  
  La valeur par défaut de cette ligne est `#define NOM_POINT_ACCES "Serre #1"`. Si vous n'avez qu'une seule serre, vous n'avez pas besoin de changer ce nom. Sinon, vous devriez changer ce nom pour que chacune de vos serres ait un nom de point d'accès unique.  
  Si le nom que vous voulez donner au point d'accès est `Serre Gatineau #3`, modifier la ligne pour la valeur suivante: `#define NOM_POINT_ACCES "Serre Gatineau #3"`.

- Configuration du Google Sheet

  Pour cette étape, vous aurez besoin des valeurs que vous avez notez plus tôt lors de la configuration de votre Google Drive.

  Vous devez d'abord modifier l'ID du GScript. La ligne à modifier commence par `#define ID_GSCRIPT`. Vous devrez remplacer l'ID présent entre les guillemets anglais par l'ID de votre GScript.  
  La valeur par défaut de la ligne est `#define ID_GSCRIPT "AKfycbzow4RKbP1QCVnKt0uSCneli87HMKISXzx6JSR8gy7IHcRXMsVY"`.  
  Si votre ID de GScript est `1IewKoCbMbIR0yS4M8K7cXVSVnzAnPzYKKcHRxHf6s7S8yiQtXRglCJu`, remplacer la ligne par la valeur suivante: `#define ID_GSCRIPT "1IewKoCbMbIR0yS4M8K7cXVSVnzAnPzYKKcHRxHf6s7S8yiQtXRglCJu"`.

  Vous devez ensuite modifier l'ID du tableur. La ligne à modifier commence par `#define ID_TABLEUR`. Vous devrez remplacer l'ID présent entre les guillemets anglais par l'ID de votre Google Sheet.  
  La valeur par défaut de la ligne est `#define ID_TABLEUR "1z8vQ3vUCyoi0sj1BVqjNvF7AQlTIBJuVAWZDBD1RBRY"`.  
  Si votre Google Sheet est `RFDyjo8V01Wu3BvIQV7qBTDjQUBNzvsZBRAYlJCvA11i`, remplacer la ligne par la valeur suivante: `#define ID_GSCRIPT "RFDyjo8V01Wu3BvIQV7qBTDjQUBNzvsZBRAYlJCvA11i"`.

  Vous devez finalement modifier le nom de la page du tableur. La ligne à modifier commence par `#define NOM_PAGE_TABLEUR`. Vous devrez remplacer le nom entre les guillemets anglais par le nom de la page dans votre tableur.  
  La valeur par défaut de la ligne est `#define NOM_PAGE_TABLEUR "Serre #1"`.  
  Si votre le nom de la page de votre tableur est `Serre Gatineau #3`, remplacer la ligne par la valeur suivante: `#define NOM_PAGE_TABLEUR "Serre Gatineau #3"`.

- Mot de passe de l'application

  Ce mot de passe est celui que vous devrez entrer dans l'application afin de pouvoir vous connecter à la serre. Assurez-vous de choisir un mot de passe sécuritaire. Utilisez des lettres minuscules, majuscules et des symboles.  
  Vous pouvez utiliser un [Gestionnaire de mot de passe](https://fr.wikipedia.org/wiki/Gestionnaire_de_mots_de_passe) ou un [Générateur de mot de passe](https://www.lastpass.com/password-generator).

  La ligne à modifier commence par `#define MOT_DE_PASSE_SUPER_USAGER`. Vous devrez remplacer le mot de passe entre les guillemets anglais par votre mot de passe sécuritaire.  
  La valeur par défaut de la ligne est `#define MOT_DE_PASSE_SUPER_USAGER "Changer ce mot de passe!"`.  
  Si votre mot de passe est `pS&FHGdwR%Cfzla&aib$`, remplacer la ligne par la valeur suivante: `#define MOT_DE_PASSE_SUPER_USAGER "pS&FHGdwR%Cfzla&aib$"`.

- Fuseau horaire et heure d'été

  Bien configurer le fuseau horaire et l'heure d'été est très important. Si l'heure est mal configurée, la température de la mauvaise période de la journée sera utilisé pour adapter les paramètres de fonctionnement du contrôleur à votre installationée, ce qui n'est pas optimal pour vos plantes.

  La ligne à modifier pour choisir le fuseau horaire commence par `#define FUSEAU_HORAIRE`. Vous devrez remplacer le décalage entre les parenthèses par celui de votre région.  
  La valeur par défaut de la ligne est `#define FUSEAU_HORAIRE (-5 * HEURE)`, le fuseau horaire de New York.  
  Si votre fuseau horaire est de -7h30min, remplacer la ligne par la valeur suivante: `#define FUSEAU_HORAIRE (-7 * HEURE - 30 * MINUTE)`.  
  Si votre fuseau horaire est de 3h15min, remplacer la ligne par la valeur suivante: `#define FUSEAU_HORAIRE (3 * HEURE + 15 * MINUTE)`.  
  Faites très attention aux signes des nombres que vous utilisez.

  La ligne à modifier pour choisir le décalage de l'heure d'été commence par `#define DECALAGE_HEURE_ETE`. Vous devrez remplacer le décalage entre les parenthèses par celui de votre région. Le décalage entré s'additionne au décalage de votre fuseau horaire pendant l'été.  
  La valeur par défaut de la ligne est `#define DECALAGE_HEURE_ETE (1 * HEURE)`.  
  Si votre fuseau horaire est de -1h15min, remplacer la ligne par la valeur suivante: `#define DECALAGE_HEURE_ETE (-1 * HEURE - 15 * MINUTE)`.  
  Si vous ne voulez pas appliquer de décalage pour l'heure d'été, remplacer la ligne par la valeur suivante: `#define FUSEAU_HORAIRE (0)`.  
  Faites très attention aux signes des nombres que vous utilisez.

[wpa]: https://fr.wikipedia.org/wiki/Wi-Fi_Protected_Access
