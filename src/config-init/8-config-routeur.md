## Configurer votre routeur

Si vous désirez vous connecter à votre contrôleur partout où vous avez une connexion à Internet, vous allez devoir configurer votre routeur. Si vous n'avez pas besoin de cela, vous pouvez ignorer cette étape.

Pour rendre votre contrôleur accessible sur Internet, vous devez faire ce qu'on appelle ouvrir un port de votre routeur. Ceci consiste à configurer votre routeur pour qu'il soit capable de rediriger les requêtes provenant d'Internet à votre contrôleur.

La manière d'ouvrir un port sur votre routeur varie en fonction du routeur que vous utilisez.  
Voici donc un [guide](https://www.linksys.com/ca/fr/support-article?articleNum=136711) pour les routeurs
Linksys. Si ceci n'est pas votre routeur, tentez de trouver le manuel d'utilisateur de votre routeur et de chercher la section de ce dernier qui explique comment effectuer un transfert de port. Votre but sera de transéférer un seul port de votre réseau vers le port `443` de votre contrôleur. Pour ce faire, vous aurez besoin de l'adresse IP de votre contrôleur.
