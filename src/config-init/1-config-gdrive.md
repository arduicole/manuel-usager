## Configurer votre Google Drive

Votre Google Drive vous permettra d'enregistrer les données de vos capteurs à long terme toutes les heures sur le nuage et d'y accéder facilement.

Avant de pouvoir configurer votre Google Drive, vous devez d'abord avoir un compte Google.  
Si vous avez un compte Gmail, Youtube ou un autre service Google, vous devriez pouvoir l'utiliser sans problèmes.  
Sinon, vous devrez vous en créer un gratuitement en suivant [ce petit guide](https://support.google.com/accounts/answer/27441?hl=fr).  
Par la suite, vous pourrez accéder à `Votre Drive`, qui est l'espace où se trouvent vos fichiers, en cliquant sur [ce lien](https://drive.google.com/drive/my-drive).

### Créer le tableur

- Créer le tableur.

  - Se rendre dans [votre Drive](https://drive.google.com/drive/my-drive).
  - Se rendre dans le dossier où vous désirez placer le tableur.
  - Faire un clique droit dans l'espace au centre de l'écran.
  - Cliquer sur le bouton pour créer un nouveau `Google Sheets`.

  **NOTE**: Un tableur déjà existant peut aussi être utilisé.

  ![Créer le tableur](../images/config_drive/creer_tableur.png)

- Nommer le tableur.

  ![Nommer le tableur](../images/config_drive/nom_tableur.png)

- Nommer la page du tableur.

  - Noter ce nom, car il sera utilisé pour configurer le contrôleur.

    **Attention**, le nom doit être noté tel quel en conservant les minuscules, les majuscules, les chiffres et les caractères spéciaux.

  ![Nommer la page](../images/config_drive/nom_page.png)

- Noter l'ID du tableur, car il sera utilisé pour configurer le contrôleur.

  L'ID du tableur est contenu dans le URL de la page du tableur. Il se trouve entre `https://docs.google.com/spreadsheets/d/` et `/edit` (sans inclure les `/`). Il est composé uniquement des caractères suivants: `a` à `z`, `A` à `Z` et `0` à `9`.  
  Ex.: Si le URL est `https://docs.google.com/spreadsheets/d/1amuDXiWB841fqeMOvc9ZhQj4o12vnQoa8Yd9ASs4Yuw/edit`, l'ID à noter est `1amuDXiWB841fqeMOvc9ZhQj4o12vnQoa8Yd9ASs4Yuw`.

  **Attention**, l'ID doit être noté tel quel en conservant les minuscules, les majuscules et les chiffres.

### Créer et publier le GScript

- À partir du tableur qui vient d'être créé, cliquer sur `Outils > Éditeur de scripts`.

  Ceci va ouvrir un éditeur de script dans un nouvel onglet.

  ![Créer le GScript](../images/config_drive/creer_gscript.png)

- Remplacer le texte du script par le contenu du fichier `TClientHistorique/GScript/InterfaceSerre.gs` dans le projet Arduino.

  - Télécharger le fichier en cliquant [ici](https://gitlab.com/arduicole/ControleurSerre/-/raw/master/TClientHistorique/GScript/InterfaceSerre.gs) si vous n'avez pas accès au projet localement.

  - Sélectionner le contenu de tout le fichier.

    Le raccourci clavier `Ctrl+A` peut être utilisé pour rapidement tout sélectionner.

  - Copier tout le texte avec le raccourci clavier `Ctrl+C`.

    ![Copier le GScript](../images/config_drive/copier_gscript.png)

  - Retourner à l'onglet de l'éditeur de script.

  - Sélectionner tout le texte de l'éditeur de script.

    Le raccourci clavier `Ctrl+A` peut être utilisé pour rapidement tout sélectionner.

  - Remplacer le texte sélectionné par le texte copié en utilisant le raccourci clavier `Ctrl+V`.

    ![Modifier le GScript](../images/config_drive/modifier_gscript.png)

- Publier le GScript.

  - Cliquer sur `Publier > Déployer en tant qu'application Web...`.

    Un dialogue va s'ouvrir.

    ![Publier le GScript](../images/config_drive/publier_gscript.png)

  - Sélectionner `Anyone, even anonymous` dans le menu déroulant du paramètre `Who has access to the app` dans le dialogue.

    **NOTE**: Au moment de l'écriture de ce manuel, certaines portions de cette page ne sont pas traduites en français.  
    ![Modifier les permissions du GScript](../images/config_drive/permissions_gscript.png)

  - Appuyer sur le bouton `Deploy`.

    ![Déployer le GScript](../images/config_drive/deployer_gscript.png)

  - Donner la permission au script d'accéder à votre compte.

    - Appuyer sur le bouton `Examiner les autorisations` de la nouvelle fenêtre qui devrait s'être ouverte.

      ![Autoriser le GScript #1](../images/config_drive/autoriser1_gscript.png)

    - Sélectionner le compte à utiliser dans la fenêtre qui devrait s'ouvrir.

      ![Autoriser le GScript #2](../images/config_drive/autoriser2_gscript.png)

    - Appuyer sur le bouton `Autoriser`.

      ![Autoriser le GScript #3](../images/config_drive/autoriser3_gscript.png)

  - Noter l'ID du GScript, il sera utilisé dans la configuration du contrôleur.

    Cet ID est contenu dans le URL affiché dans la boîte de dialogue sous le texte `Current web app URL`. Il se situe entre `https://script.google.com/macros/s/` et `/exec` (sans inclure les `/`) dans le URL. Il est composé uniquement des caractères suivants: `a` à `z`, `A` à `Z` et `0` à `9`.  
    Ex.: Si le URL est `https://script.google.com/macros/s/AKfycbxdI3lgTIqg3KfYSLmeJkZidCCCM7OybZhYuU5jArD5gxPTh8in/exec`, l'ID à noter est `AKfycbxdI3lgTIqg3KfYSLmeJkZidCCCM7OybZhYuU5jArD5gxPTh8in`.

    **Attention**, l'ID doit être noté tel quel en conservant les minuscules, les majuscules et les chiffres.

    ![ID du GScript](../images/config_drive/id_gscript.png)
