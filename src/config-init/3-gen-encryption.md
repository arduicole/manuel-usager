## Générer une clé d'encryption

Si votre contrôleur a été installé par Arduicole, une clé d'encryption valide pendant 30 ans devrait avoir été générée pour vous. Vous pouvez sauter cette étape et la réutiliser si elle n'est pas expirée.  
Si vous n'avez pas utilisé les services d'Arduicole pour votre installation ou que vous n'avez plus le code source original qu'ils vous ont fourni, suivez les instructions ci-dessous pour vous générer une nouvelle clé.

**ATTENTION!** Une clé d'encryption utilisée pour le développement accompagne le code source, mais l'utiliser rendrait votre contrôleur vulnérable à des cyber-attaques. La clé que vous allez générer sera unique et doit absolument rester secrète. Si quelqu'un obtient cette clé, veuillez suivre de nouveau les étapes ci-dessous pour vous en créer une nouvelle, recompiler le code et le retéléverser sur votre contrôleur. Si vous ne faites pas cela, la personne possédant votre clé pourrait voler votre mot de passe et changer la configuration de votre contrôleur de manière à vous faire perdre votre récolte.

- Installer [OpenSSL][openssl].

  Sur Windows, vous devez d'abord installer [winget] ou [chocolatey].  
  Si vous avez choisi [winget], exécuter la commande `winget install openssl` dans un terminal pour installer [OpenSSL][openssl].  
  Sinon, si vous avez choisi [chocolatey], exécuter la commande `choco install openssl`.

  Sur MacOS, vous aurez besoin d'installer [brew](https://brew.sh/).  
  Une fois que c'est fait, exécuter la commande `brew install openssl` pour installer [OpenSSL][openssl].

  Sur Linux, l'installation d'[OpenSSL][openssl] va dépendre de votre distribution. Voici des exemples de commandes pour différentes distributions:

  - [Debian](https://www.debian.org/) ([Ubuntu](https://ubuntu.com), [Linux Mint](https://linuxmint.com/)): `sudo apt-get install openssl`
  - [RedHat](https://www.redhat.com/en) ([Fedora](https://getfedora.org), [CentOS](https://www.centos.org/)): `sudo yum install openssl`
  - [Arch Linux](https://www.archlinux.org/) ([Manjaro](https://manjaro.org)): `sudo pacman -S openssl`

- Ouvrir un terminal dans le dossier `TServeur` du code source Arduino.
- Exécuter le script pour générer les clés d'encryption.

  Sur Windows, exécuter la ligne suivante:

  ```sh
  generer-cles.bat
  ```

  Sur MacOS et Linux, exécuter la ligne suivante:

  ```sh
  bash generer-cles.sh
  ```

  Pendant l'exécution du script, certaines informations vous seront demandées. Vous pouvez les fournir ou les ignorer en appuyant sur la touche entrée jusqu'à la fin du script.

[chocolatey]: https://chocolatey.org/docs/installation
[openssl]: https://www.openssl.org/
[winget]: https://github.com/microsoft/winget-cli
