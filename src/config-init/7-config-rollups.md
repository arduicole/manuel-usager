## Configurer les rollups

Vous devez de nouveau éditer `Config.h` pour modifier la configuration des rollups.

![](../images/config_rollups.png)

- Configurer le temps de montée des rollups.

  La ligne à modifier pour le premier rollup commence par `#define DUREE_OUVERTURE_ROLLUP_1`. Celle du deuxième rollup commence par `#define DUREE_OUVERTURE_ROLLUP_2`. Vous devrez remplacer le délai actuel par le délai que vous avez mesuré pendant votre test du mode manuel. Vous devrez inscrire ce délai en millisecondes. Pour convertir des secondes en millisecondes, il faut les multiplier par 1000. Si le délai est de 15.35s, cela donne 15350 millisecondes.  
  La valeur par défaut de la ligne du premier rollup est `#define DUREE_OUVERTURE_ROLLUP_1 10000`.  
  Si le délai que vous avez mesuré pour le premier rollup est de 9.3s, remplacer la ligne par la valeur suivante: `#define DUREE_OUVERTURE_ROLLUP_1 9300`.

- Configurer le temps de descente des rollups.

  La ligne à modifier pour le premier rollup commence par `#define DUREE_FERMETURE_ROLLUP_1`. Celle du deuxième rollup commence par `#define DUREE_FERMETURE_ROLLUP_2`. Vous devrez remplacer le délai actuel par le délai que vous avez mesuré en millisecondes pendant votre test du mode manuel.  
  La valeur par défaut de la ligne du premier rollup est `#define DUREE_FERMETURE_ROLLUP_1 8000`.  
  Si le délai que vous avez mesuré pour le premier rollup est de 7.5s, remplacer la ligne par la valeur suivante: `#define DUREE_OUVERTURE_ROLLUP_1 7500`.

- Configurer le délai de changement de direction des rollups.

  Le délai de changement de direction est déterminé par le temps d'arrêt des rollups que vous avez mesuré. Ce délai aide le contrôleur à augmenter la précision du calcul de la position des rollups et à augmenter la durée de vie de ses relais. Il n'y a aucune conséquence à ce que ce délai soit trop long, mais il ne faut pas qu'il soit trop court.  
  C'est pour cela que vous devriez ajouter un marge de sécurité de 1s à la valeur que vous avez mesuré. Par exemple, si vous avez mesuré 1.5s, utilisez 2.5s comme délai de changement de direction.

  La ligne à modifier pour le premier rollup commence par `#define DELAI_CHANGEMENT_DIRECTION_ROLLUP_1`. Celle du deuxième rollup commence par `#define DELAI_CHANGEMENT_DIRECTION_ROLLUP_2`. Vous devrez remplacer le délai actuel par le délai que vous avez mesuré en millisecondes pendant votre test du mode manuel plus une seconde de marge de sécurité.  
  La valeur par défaut de la ligne du premier rollup est `#define DELAI_CHANGEMENT_DIRECTION_ROLLUP_1 5000`.  
  Si le délai que vous avez mesuré pour le premier rollup est de 1.5s, donc 2.5s avec la marge de sécurité, remplacer la ligne par la valeur suivante: `#define DUREE_OUVERTURE_ROLLUP_1 2500`.

- [Compiler et téléverser le code](./5-compiler-televerser.md)
