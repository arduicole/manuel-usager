# Introduction

Ce contrôleur de serre simplifie la tâche demandante de l'agriculteur tout en augmentant le rendement des cultures en asservissant automatiquement la température d'une serre.

Ce document a pour but de vous faire découvrir les différentes fonctionnalités de votre contrôleur et de vous apprendre à les utiliser pour que vous puissiez en tirer le maximum.
