## Configurer une nouvelle serre

<style>
.content img {
  display: block;
  margin-right: auto;
  margin-left: auto;
  max-height: 85vh;
}
</style>

- Appuyer sur le bouton flottant avec un `+` dans la page principale.

  ![](../images/app/config_serre_1.png)

  Un dialogue devrait s'ouvrir vous permettant de configurer la nouvelle serre.

- Configurer son nom.

  Vous pouvez utiliser n'importe quel nom pour votre serre. Cependant, chaque serre dans l'application sur votre téléphone doit avoir un nom unique.  
  Si vous configurez la même serre sur plusieurs téléphones, elle n'a pas besoin d'avoir le même nom sur tous ces téléphones.

- Configurer son URL.

  À moins que vous ayez configuré un nom de domaine pour votre réseau, ceci sera probablement une adresse IP.  
  Il y a 2 types d'adresses IP:

  - [IPv4](https://wikipedia.org/wiki/IPv4)

    Une suite de 4 nombres entre 0 et 255 séparés par des points.  
    Ex.: `192.168.0.107`.

  - [IPv6](https://wikipedia.org/wiki/IPv6)

    Une suite de 8 groupes de 4 [nombres hexadécimaux](https://fr.wikipedia.org/wiki/Syst%C3%A8me_hexad%C3%A9cimal) entre `0000` et `ffff` séparés par des deux-points.  
    Ex.: `d4f9:49a8:5564:4330:94b7:6146:bcd8:d232`.

  Vous devriez avoir obtenu l'IP de votre contrôleur pendant sa configuration.

- Configurer son port.

  À moins d'avoir changé la propriété `PORT_SERVEUR` dans `Config.h`, le port devrait être `443`. Sinon, ce sera la valeur que vous avez assignée à `PORT_SERVEUR`.

- Configurer le nom de son point d'accès.

  Le nom du point d'accès est celui que vous avez configuré à l'aide de la propriété `NOM_POINT_ACCES` dans `Config.h`.

- Configurer l'IP de son point d'accès.

  À moins que vous n'ayez modifié le code source, la valeur à entrer ici est `192.168.4.1`.

- Configurer le port de son point d'accès.

  À moins d'avoir changé la propriété `PORT_SERVEUR` dans `Config.h`, le port devrait être `443`. Sinon, ce sera la valeur que vous avez assignée à `PORT_SERVEUR`.

  ![](../images/app/config_serre_2.png)

- Obtenir son certificat SSL pour l'encryption.

  Cette étape est cruciale. Elle permet d'obtenir les informations nécessaires du contrôleur pour sécuriser vos communications avec ce dernier.

  **ATTENTION!** Si vous effectuez cette étape sur un réseau avec des pirates informatiques, ils pourront imiter votre serre et obtenir votre mot de passe pour se connecter à vos serres. Ils utiliseront ensuite ce mot de passe pour se connecter à votre serre et possiblement ruiner votre récolte.  
  Ce type d'attaque s'appelle l'[attaque de l'homme du milieu](https://fr.wikipedia.org/wiki/Attaque_de_l%27homme_du_milieu).  
  Après que vous ayez effectué cette étape, même si vous allez sur un réseau avec des pirates, ils ne devraient pas être capables de vous voler votre mot de passe.

  ![](../images/app/config_serre_3.png)

  Si le certificat a été obtenu avec succès, vous verrez une suite de caractères «aléatoires» apparaître dans le champ de texte `Certificat:`.

- Sauvergarder la serre.

  ![](../images/app/config_serre_4.png)

- Appuyer sur la serre créée.

  Ceci va vous permettre de voir les informations de la serre plus en détail.

  ![](../images/app/selectionner_serre.png)
