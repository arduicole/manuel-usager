# Application mobile

- [Installation et configuration initiale](./config-init.md)
- [Configurer une nouvelle serre](./config-serre.md)
- [Mode de contrôle à distance](./ctrl-distance/index.md)

  - [Configurer et activer](./ctrl-distance/config-et-activer.md)
  - [Désactiver ce mode](./ctrl-distance/desactiver.md)

- [Mode de cycle](./cycle/index.md)

  - [Créer un cycle](./cycle/creer.md)
  - [Modifier un cycle](./cycle/modifier.md)
  - [Exécuter un cycle](./cycle/executer.md)
  - [Visionner un cycle en exécution](./cycle/visionner.md)
  - [Arrêter un cycle en exécution](./cycle/arreter.md)

- [Mode d'asservissement](./asservissement/index.md)

  - [Activer](./asservissement/activer.md)
  - [Configurer le niveau d'ensoleillement](./asservissement/config-ensoleillement.md)
  - [Configurer une consigne](./asservissement/config-consigne.md)

- [Alertes](./alertes/index.md)

  - [Configurer l'intervalle de vérification des alertes](./alertes/config-verif.md)
  - [Configurer les notifications](./alertes/config-notifs.md)
  - [Voir la liste d'alertes](./alertes/visionner.md)
  - [Supprimer des alertes](./alertes/supprimer.md)
