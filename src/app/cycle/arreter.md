### Arrêter un cycle en exécution

<style>
.content img {
  display: block;
  margin-right: auto;
  margin-left: auto;
  max-height: 85vh;
}
</style>

Pour arrêter un cycle en exécution, il faut qu'un cycle soit en cours d'exécution.

- Sélectionner la serre dont vous voulez arrêter le cycle dans la liste sur la page d'accueil.

  ![](../../images/app/selectionner_serre.png)

- Faire défiler l'onglet `Général` jusqu'en bas.
- Appuyer sur le bouton `Arrêter cycle`.

  ![](../../images/app/cycle_arreter.png)
