### Exécuter un cycle

<style>
.content img {
  display: block;
  margin-right: auto;
  margin-left: auto;
  max-height: 85vh;
}
</style>

- Sélectionner la serre pour laquelle exécuter un cycle dans la liste sur la page d'accueil.

  ![](../../images/app/selectionner_serre.png)

- Appuyer sur l'onglet `Cycle` vers le haut de l'écran.

  ![](../../images/app/onglet_cycle.png)

- Sélectionner le cycle à exécuter dans le menu déroulant.

  ![](../../images/app/cycle_selectionner.png)

- Appuyer sur le bouton `Exécuter`.

  ![](../../images/app/cycle_executer.png)
