### Visionner un cycle en exécution

<style>
.content img {
  display: block;
  margin-right: auto;
  margin-left: auto;
  max-height: 85vh;
}
</style>

- Sélectionner la serre dont vous voulez visionner le cycle dans la page d'accueil.

  ![](../../images/app/selectionner_serre.png)

- Faire défiler l'onglet `Général` jusqu'en bas.
- Visionner le cycle en exécution.

  Si un cycle est en exécution, vous verrez ses caractéristiques et son avancement. Ceci inclut l'étape qui est en cours, les durées de ventilation et de chauffage et le nombre de répétitions effectuées.

  Si aucun cycle n'est en exécution, vous verrez des points d'exclamation plutôt que les informations du cycle.

  ![](../../images/app/cycle_execution.png)
