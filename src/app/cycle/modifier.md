### Modifier un cycle

<style>
.content img {
  display: block;
  margin-right: auto;
  margin-left: auto;
  max-height: 85vh;
}
</style>

- Sélectionner n'importe quelle serre dans la liste sur la page d'accueil.

  Les cycles sont communs à toutes les serres, donc vous n'avez pas besoin de vous soucier de sélectionner une serre particulière.

  ![](../../images/app/selectionner_serre.png)

- Appuyer sur l'onglet `Cycle` vers le haut de l'écran.

  ![](../../images/app/onglet_cycle.png)

- Sélectionner le cycle à modifier dans le menu déroulant.

  ![](../../images/app/cycle_selectionner.png)

- Modifier le cycle.

  Vous pouvez maintenant entrer les informations suivantes pour le cycle:

  - Durée de chauffage: intervalle de temps en minutes à passer à chauffer la serre pour chaque répétition du cycle
  - Durée de ventilation: intervalle de temps en minutes à passer à ventiler la serre pour chaque répétition du cycle
  - Répétitions: nombre de fois que les périodes de chauffage et de ventilation doivent se répéter. Un cycle doit avoir au moins une répétition.
  - Commencer par: étape, ventilation ou chauffage, par laquelle le cycle doit débuter.

  Ex.: Si vous configurez un cycle avec une durée de chauffage de 2min, une durée de ventilation de 5min, 2 répétitions et qui commence par l'étape de ventilation, le contrôleur va commencer par ventiler la serre pendant 5min, puis il va la chauffer pendant 2min, puis il va la ventiler pendant 5min, puis il va la chauffer pendant 2min.

- Appuyer sur le bouton `Enregistrer`.

  ![](../../images/app/cycle_enregistrer.png)
