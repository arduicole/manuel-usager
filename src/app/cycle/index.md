## Mode de cycle

Le mode de cycle permet d'exécuter une suite de périodes de chauffage et de ventilation un certain nombre de fois. La période de chauffage et la période de ventilation peuvent être réduites à 0 afin d'uniquement chauffer ou ventiler la serre pendant une période donnée.

- [Créer un cycle](./creer.md)
- [Modifier un cycle](./modifier.md)
- [Exécuter un cycle](./executer.md)
- [Visionner un cycle en exécution](./visionner.md)
- [Arrêter un cycle en exécution](./arreter.md)
