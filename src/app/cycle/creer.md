### Créer un cycle

<style>
.content img {
  display: block;
  margin-right: auto;
  margin-left: auto;
  max-height: 85vh;
}
</style>

- Sélectionner n'importe quelle serre dans la liste sur la page d'accueil.

  Les cycles sont communs à toutes les serres, donc vous n'avez pas besoin de vous soucier de sélectionner une serre particulière.

  ![](../../images/app/selectionner_serre.png)

- Appuyer sur l'onglet `Cycle` vers le haut de l'écran.

  ![](../../images/app/onglet_cycle.png)

- Appuyer sur le bouton `Ajouter`.

  ![](../../images/app/cycle_ajouter.png)

  Ceci va ouvrir un dialogue pour lui donner un nom.

- Nommer le cycle et le sauvegarder.

  Chaque cycle doit avoir un nom unique.

  ![](../../images/app/cycle_nommer.png)
