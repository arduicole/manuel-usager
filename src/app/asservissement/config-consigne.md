### Configurer les consignes

<style>
.content img {
  display: block;
  margin-right: auto;
  margin-left: auto;
  max-height: 85vh;
}
</style>

- Sélectionner une serre dans la page principale pour accéder à ses détails et paramètres.

  ![](../../images/app/selectionner_serre.png)

  Si c'est la première fois que vous visitez cette page, un dialogue va s'ouvrir pour vous demander de créer une nouvelle consigne pour votre serre. La consigne est la configuration du mode d'asservissement.  
  Vous pouvez choisir de créer une nouvelle consigne ou d'utiliser une consigne existante, si vous avez déjà configuré des consignes pour d'autres serres.

  Ce dialogue va également s'ouvrir si la consigne de la serre a été changée pour une raison ou une autre.

- Choisir le niveau d'ensoleillement de la journée.

  La température idéale pour une plante varie en fonction du niveau d'ensoleillement.  
  Tous les jours, vous devrez effectuer cette étape pour que le contrôleur puisse savoir quelle température il doit asservir pour la journée.

  Vous pouvez choisir entre 3 niveaux d'ensoleillement:

  - Ensoleillé
  - Nuageux
  - Mi-nuageux

  Si vous avez plusieurs serres, vous pouvez effectuer cette étape pour une de vos serres, puis appuyer sur le bouton `Appliquer à toutes serres` pour que toutes vos serres aient le même niveau d'ensoleillement pour la journée. Cliquez [ici](https://gitlab.com/arduicole/ControleurSerre/-/issues/54) pour en savoir plus.

  ![](../../images/app/consigne_ensoleillement.png)

- Appuyer sur l'onglet `Consigne` vers le haut de l'écran.

  Vous pouvez également naviguer d'un onglet à l'autre en balayant l'écran d'un côté à l'autre.  
  C'est à cet endroit que vous avez accès aux paramètres de la consigne.

  ![](../../images/app/serre_consigne.png)

- Appuyer sur le bouton `Modifier`.

  Ceci débloque tous les champs de texte de cet onglet ce qui vous permet de les éditer.

  ![](../../images/app/consigne_modifier.png)

- Modifier les consignes.

  Les consignes sont les températures que le mode d'asservissement va tenter de maintenir en fonction de la température et de l'heure de la journée. Elles se trouvent dans le tableau de l'onglet sur lequel vous vous trouvez.

  Chaque colonne du tableau représente l'ensoleillement de la journée.

  Chaque rangée du tableau de consignes représente la période de la journée:

  - Pré-jour: minuit à 6h
  - Jour: 6h à midi
  - Pré-nuit: midi à 17h
  - Nuit: 17h à minuit

  Les périodes de la journée ont des heures fixes pour l'instant, mais ces heures seront éventuellement configurables et pourront aussi être obtenues par météo. Cliquez [ici](https://gitlab.com/arduicole/app/-/issues/22) pour plus de détails.

- Appuyer sur le bouton `Enregistrer`.

  Ceci enregistre la consigne dans l'application.

  ![](../../images/app/consigne_enregistrer.png)

- Appuyer sur le bouton `Appliquer`.

  Ceci envoi la consigne à la serre.

  ![](../../images/app/consigne_appliquer.png)
