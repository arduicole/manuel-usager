### Activer

<style>
.content img {
  display: block;
  margin-right: auto;
  margin-left: auto;
  max-height: 85vh;
}
</style>

- Si le mode manuel est activé, utiliser l'interrupteur du boîtier du contrôleur pour désactiver le mode manuel.
- Si le [mode de contrôle à distance](../ctrl-distance/index.md) est activé, le désactiver à partir de la page de contrôle à distance de l'application.
- Si le [mode de cycle](../cycle/index.md) est activé, arrêter le cycle en cours à partir de l'onglet général de la page de détails de la serre.

Vous aurez la confirmation que vous avez désactivé tous les modes si le mode affiché dans la page de détails de la serre indique que le mode est `Asservissement`.

![](../../images/app/serre_asservissement.png)
