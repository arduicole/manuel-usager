### Configurer le niveau d'ensoleillement

<style>
.content img {
  display: block;
  margin-right: auto;
  margin-left: auto;
  max-height: 85vh;
}
</style>

- Sélectionner une serre dans la page principale pour accéder à ses détails et paramètres.

  ![](../../images/app/selectionner_serre.png)

- Choisir le niveau d'ensoleillement de la journée.

  La température idéale pour une plante varie en fonction du niveau d'ensoleillement.  
  Tous les jours, vous devrez effectuer cette étape pour que le contrôleur puisse savoir quelle température il doit asservir pour la journée.  
  Éventuellement, le contrôleur sera capable d'aller lui-même chercher la météo ce qui simplifiera votre tâche.

  Vous pouvez choisir entre 3 niveaux d'ensoleillement:

  - Ensoleillé
  - Nuageux
  - Mi-nuageux

  Si vous avez plusieurs serres, vous pouvez effectuer cette étape pour une de vos serres, puis appuyer sur le bouton `Appliquer à toutes serres` pour que toutes vos serres aient le même niveau d'ensoleillement pour la journée.

  ![](../../images/app/consigne_ensoleillement.png)
