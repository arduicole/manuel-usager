## Mode de contrôle à distance

Le mode de contrôle à distance permet de manuellement choisir l'état de chaque actionneur. Pour pouvoir l'activer, il faut que le mode manuel soit désactivé.

- [Configurer et activer](./config-et-activer.md)
- [Désactiver ce mode](./desactiver.md)
