### Désactiver ce mode

<style>
.content img {
  display: block;
  margin-right: auto;
  margin-left: auto;
  max-height: 85vh;
}
</style>

- Sélectionner une serre dans la page principale pour accéder à ses détails et paramètres.

  ![](../../images/app/selectionner_serre.png)

- Appuyer sur le bouton `Contrôle à distance` dans la section `Actionneurs`.

  ![](../../images/app/ctrl_distance_consulter.png)

- Attendre que les informations sur les actionneurs soient reçues.

- Appuyer sur l'interrupteur pour désactiver le mode.

  Pour pouvoir appuyer dessus, il faut que le mode soit activé. Si l'interrupteur est gris, cela signifie que le mode est déjà désactivé.

  ![](../../images/app/ctrl_distance_desactiver.png)
