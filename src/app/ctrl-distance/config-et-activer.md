### Configurer et activer

<style>
.content img {
  display: block;
  margin-right: auto;
  margin-left: auto;
  max-height: 85vh;
}
</style>

- Sélectionner une serre dans la page principale pour accéder à ses détails et paramètres.

  ![](../../images/app/selectionner_serre.png)

- Appuyer sur le bouton `Contrôle à distance` dans la section `Actionneurs`.

  ![](../../images/app/ctrl_distance_consulter.png)

- Attendre que les informations sur les actionneurs soient reçues.

- Choisir l'état de chaque actionneur.

  Pour le ventilateur et la fournaise, il suffit d'appuyer sur le bouton à bascule pour choisir de les allumer ou non.

  Pour les moteurs de rollups, il faut choisir leur pourcentage d'ouverture. À moins que vous ayez changé les paramètres `HYSTERESIS_ROLLUP_1` ou `HYSTERESIS_ROLLUP_2`, les rollups n'atteindront pas exactement la consigne. Ils vont se déplacer à des positions à 5% près des consignes données. Si les consignes sont de 0 ou 100%, les rollups se déplaceront cependant à ces positions avec précision.

  ![](../../images/app/ctrl_distance_config.png)

- Appuyer sur le bouton flottant d'envoi.

  Vous pouvez répéter l'étape précédente ainsi que celle-ci pour changer les consignes configurées. Vous n'avez pas besoin de désactiver le mode de contrôle à distance pour pouvoir le reconfigurer et l'activer.

  ![](../../images/app/ctrl_distance_envoi.png)

- Appuyer sur le bouton flottant de rafraîchissement.

  Ceci vous permettra de mettre à jour l'état de tous les actionneurs incluant les rollups qui se déplacent vers leurs nouvelles consignes.

  Comme pour beaucoup d'autres pages dans l'application, le rafraîchissement se fait automatiquement à un intervalle configuré dans les paramètres de l'application. Cependant, utiliser ce bouton permet de rapidement rafraîchir les données au besoin sans modifier les paramètres.

  ![](../../images/app/ctrl_distance_rafraichir.png)
