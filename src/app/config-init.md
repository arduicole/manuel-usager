## Installation et configuration initiale de l'application

<style>
.content img {
  display: block;
  margin-right: auto;
  margin-left: auto;
  max-height: 85vh;
}
</style>

- Télécharger l'application disponible ici: <http://arduicole.com/apk>.

  Téléchargez-là directement à partir du navigateur de votre choix sur le téléphone mobile sur lequel vous désirez installer l'application.  
  Il sera peut-être nécessaire de donner l'autorisation à votre navigateur d'installer des applications provenant de sources inconnues.

  |                 1                  |                      2                       |
  | :--------------------------------: | :------------------------------------------: |
  | ![](../images/app/telecharger.png) | ![](../images/app/ouvrir_telechargement.png) |

  |                    3                     |                    4                     |
  | :--------------------------------------: | :--------------------------------------: |
  | ![](../images/app/source_inconnue_1.png) | ![](../images/app/source_inconnue_2.png) |

  |                5                 |                    6                    |
  | :------------------------------: | :-------------------------------------: |
  | ![](../images/app/installer.png) | ![](../images/app/ouvrir_installee.png) |

- Ouvrir l'application et lui accorder les permissions demandées.

  Il est nécessaire de donner à l'application la permission d'obtenir votre localisation, même si elle ne l'utilise pas. Cette autorisation est nécessaire pour obtenir le nom du réseau WiFi sur lequel vous êtes connecté afin de pouvoir choisir le bon URL et le bon port pour se connecter aux serres.  
  Android requiert cette autorisation pour obtenir le nom du réseau WiFi, car certaines applications utilisent le nom du réseau WiFi de l'utilisateur pour déterminer sa localisation sans qu'il ne le sache, mais ce n'est pas le cas de cette application.

  ![](../images/app/permissions.png)

- Configurer le mot de passe des serres à partir des paramètres.

  Utiliser le mot de passe que vous avez configuré dans le fichier `Config.h` du code source de vos serres. Les minuscules, les majuscules et les symboles doivent être identiques au mot de passe écrit dans `Config.h`.

  |                  1                   |                  2                  |
  | :----------------------------------: | :---------------------------------: |
  | ![](../images/app/ouvrir_params.png) | ![](../images/app/params_mdp_1.png) |

  |                  3                  |
  | :---------------------------------: |
  | ![](../images/app/params_mdp_2.png) |
