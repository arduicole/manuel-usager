### Supprimer des alertes

<style>
.content img {
  display: block;
  margin-right: auto;
  margin-left: auto;
  max-height: 85vh;
}
</style>

- [Voir la liste d'alertes](./visionner.md).

- Balayer une alerte de droite à gauche.

  Ceci supprime l'alerte balayée.

  |                      1                       |                      2                       |
  | :------------------------------------------: | :------------------------------------------: |
  | ![](../../images/app/supprimer_alerte_1.png) | ![](../../images/app/supprimer_alerte_2.png) |

- Balayer une alerte de gauche à droite.

  Ceci supprime l'alerte balayée et toutes les alertes qui la précède.

  |                           1                           |                           2                           |
  | :---------------------------------------------------: | :---------------------------------------------------: |
  | ![](../../images/app/supprimer_vielles_alertes_1.png) | ![](../../images/app/supprimer_vielles_alertes_2.png) |
