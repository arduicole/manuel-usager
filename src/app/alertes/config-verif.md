### Configurer l'intervalle de vérification des alertes

<style>
.content img {
  display: block;
  margin-right: auto;
  margin-left: auto;
  max-height: 85vh;
}
</style>

- Ouvrir les paramètres de l'application.

  ![](../../images/app/ouvrir_params.png)

- Choisir l'intervalle entre 30s et 600s (10min).

  ![](../../images/app/params_interval_alertes.png)

  Il est à noter que lorsque votre téléphone se [met en veille](https://developer.android.com/training/monitoring-device-state/doze-standby), ce qui arrive quand votre écran est éteint pendant une certaine période de temps et que votre téléphone n'est pas en charge, l'intervalle de vérification des alertes va augmenter. Ceci est dû aux stratégies d'optimisation de batterie d'Android.  
  Nous avons cependant déjà une solution à ce problème qui sera appliquée dans un futur proche. Cliquez [ici](https://gitlab.com/arduicole/app/-/issues/38) pour en savoir plus.
