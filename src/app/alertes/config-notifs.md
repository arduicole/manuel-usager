### Configurer les notifications

<style>
.content img {
  display: block;
  margin-right: auto;
  margin-left: auto;
  max-height: 85vh;
}
</style>

- Ouvrir les paramètres de l'application.

  ![](../../images/app/ouvrir_params.png)

- Appuyer sur la section `Notifications`.

  ![](../../images/app/params_notifs.png)

  Ceci va ouvrir les paramètres de notifications de l'application.

- Appuyer sur le type de notification que vous voulez configurer.

  Les notifications sont séparées en 4 types que vous pouvez tous configurer différemment.  
  Les 3 premiers sont pour les alertes de différents niveaux d'urgence.  
  Le dernier est pour la notification de déconnexion de la serre.

  ![](../../images/app/params_selectionner_notif.png)

- Appuyer sur le bouton pour voir les paramètres avancés.

  ![](../../images/app/params_avances_notif.png)

- Configurer la notification.

  Vous pouvez configurer toutes les notifications comme vous le voulez.  
  Par défaut, toutes les notifications:

  - Sont activées.
  - Sonnent et s'affichent sur votre écran.
  - Utilisent la sonnerie de notification par défaut.
  - Affichent un point de notification sur votre écran d'accueil.
  - Ne s'affichent pas lorsque vous êtes en mode `Ne pas déranger`.

  Nous vous recommandons de permettre aux notifications d'alertes d'urgence élevée et de déconnexion de la serre de s'afficher en mode `Ne pas déranger`.  
  **ATTENTION!** Même si vous n'activez jamais le mode `Ne pas déranger`, par défaut Android l'active automatiquement pendant la nuit pour ne pas que votre téléphone ne sonne sans raison.

  ![](../../images/app/params_config_notif.png)
