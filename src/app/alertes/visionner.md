### Voir la liste d'alertes

<style>
.content img {
  display: block;
  margin-right: auto;
  margin-left: auto;
  max-height: 85vh;
}
</style>

- Sélectionner une serre dans l'écran principal de l'application.

  ![](../../images/app/selectionner_serre.png)

- Sélectionner l'onglet `Alertes`.

  ![](../../images/app/onglet_alertes.png)

- Observer la liste d'alertes.

  Les alertes de la listes sont classées en fonction de leur date. L'alerte la plus récente est tout en haut de la liste.  
  Les alertes sont aussi classées en groupe en fonction du jour pendant lequel elles sont survenues.

- Appuyer sur une alerte.

  Le titre et la description de certaines alertes peuvent être assez longs. Lorsque c'est le cas, ils sont terminés par `…`. Pour voir le titre et la description complète d'une alerte, vous n'avez qu'à appuyer dessus.

  |                     1                      |                     2                      |
  | :----------------------------------------: | :----------------------------------------: |
  | ![](../../images/app/details_alerte_1.png) | ![](../../images/app/details_alerte_2.png) |
