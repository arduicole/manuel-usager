## Alertes

L'application se connecte à vos contrôleurs de serres à un intervalle donné pour vérifier si des alertes sont survenues. Lorsqu'elle parvient à se connecter au contrôleur, elle crée des notifications pour chaque alerte. Sinon, elle crée une notification signalant qu'elle n'est pas parvenue à se connecter à votre serre.

Les alertes de déconnexion ne sont pas à prendre à la légère. Si vous êtes chanceux, votre téléphone ou votre contrôleur s'est seulement déconnecté d'Internet. Si vous êtes mal chanceux, il n'est plus alimenté et ne peut donc plus surveiller et contrôler la température de votre serre.

Les notifications d'alertes sont séparées en 3 niveaux d'urgence:

- Bas:

  Ce sont des urgences que vous pouvez pratiquement ignorer.  
  Lorsque le contrôleur ne parvient pas à enregistrer les données de ses capteurs sur Google Drive, c'est une alerte de ce niveau qui est créée.

- Moyen:

  Ces urgences sont plus importantes, mais elles ne requièrent pas votre attention immédiate.  
  Ceci inclut les erreurs de lecture d'un des capteurs, car le système peut continuer de fonctionner sans problème avec un seul capteur, mais vous aurez quand même besoin de régler ce problème pour que le système fonctionne de manière optimale.

- Élevé:

  Ces urgences requièrent possiblement votre attention immédiate.  
  Ceci inclut les situations suivantes:

  - Les deux capteurs ont cessé de fonctionner.
  - La température a dépassé un seuil critique.

- [Configurer l'intervalle de vérification des alertes](./config-verif.md)
- [Configurer les notifications](./config-notifs.md)
- [Voir la liste d'alertes](./visionner.md)
- [Supprimer des alertes](./supprimer.md)
