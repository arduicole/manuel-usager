# [Guide d'utilisateur][site]

[![pipeline status](https://gitlab.com/chapeau-melon/manuel-usager/badges/master/pipeline.svg)](https://gitlab.com/chapeau-melon/manuel-usager/-/commits/master)
[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg)](https://github.com/prettier/prettier)

Ce guide explique les caractéristiques du [contrôleur de serre](https://gitlab.com/chapeau-melon/ControleurSerre) et comment l'utiliser en conjonction avec l'[application mobile](https://gitlab.com/chapeau-melon/app).

Vous pouvez consulter ce manuel et le télécharger en format PDF sur ce [site][site].

## Contribuer à ce projet

Ce projet utilise l'outil [mdBook][mdbook] pour nous permettre d'écrire ce manuel d'usager en [Markdown][cmark]. Je vous invite donc à [apprendre le Markdown](https://commonmark.org/help/tutorial/) et à survoler le [manuel d'usager de mdBook][mdbook] avant de contribuer à ce projet.

### Télécharger et installer les outils de développement

En plus d'avoir besoin de [mdBook], vous aurez besoin de télécharger [mdBook LinkCheck] qui permet de vérifier que tous les liens dans [mdBook] sont valides. Il sera donc impossible pour ce manuel de pointer vers des URLs qui sont invalides.

Peu importe votre plateforme, vous pouvez télécharger les dernières versions des exécutables pour [mdBook] et [mdBook LinkCheck] ici:

- [mdBook](https://github.com/rust-lang/mdBook/releases)
- [mdBook LinkCheck](https://github.com/Michael-F-Bryan/mdbook-linkcheck/releases)

Assurez-vous de les placer dans un endroit accessible dans votre `PATH`.

### Utiliser mdBook

Pendant que vous éditez les fichiers [Markdown][cmark], vous pouvez voir en temps réel, chaque fois que vous enregistrez vos fichiers, le résultat dans votre navigateur. Pour ce faire, entrez la commande suivante dans votre terminal:

```sh
mdbook serve
```

Vous pourrez ensuite voir le résultat en allant au URL <http://localhost:3000> dans votre navigateur. Votre page de navigateur devrait se rafraîchir chaque fois que vous enregistrez les fichiers [Markdown][cmark] que vous avez édités.  
Si ce n'est pas le cas, il se peut que vous ayez fait une erreur. Vous devrez alors allez inspecter les messages d'erreurs dans votre terminal et corriger ce qui est demandé dans les fichiers du projet.

## Formatter le code avec [prettier]

Ce projet utilise [prettier] (avec sa configuration par défaut) pour le formatage de **tous** ses fichiers. Pour l'utiliser, vous pouvez utiliser une des méthodes suivantes:

- [Extension pour Atom](https://atom.io/packages/prettier-atom)
- [Extension pour VSCode](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)
- [CLI](https://prettier.io/docs/en/install.html)

Il est important de bien formater vos fichiers, sinon le CI va échouer.

[prettier]: https://prettier.io/
[site]: https://chapeau-melon.gitlab.io/manuel-usager/
[mdbook]: https://rust-lang.github.io/mdBook/
[mdbook linkcheck]: https://github.com/Michael-F-Bryan/mdbook-linkcheck/
[cmark]: https://commonmark.org/
